<?php

require_once './animal.php';

class Ape extends animal
{
	public $yell = "Auooo";

	public function yell() {
		return $this->yell;
	}
} 